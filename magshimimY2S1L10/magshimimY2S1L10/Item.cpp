#include "Item.h"

Item::Item(std::string name, std::string serial_num, double unitPrice) : _serialNumber(serial_num), _unitPrice(unitPrice), _name(name), _count(1)
{
}

Item::Item()
{
}

Item::~Item()
{
}

double Item::totalPrice() const
{
	return this->_count*this->_unitPrice;
}

bool Item::operator<(const Item& other) const
{
	return this->getSerialNumber() < other.getSerialNumber();
}

bool Item::operator>(const Item& other) const
{
	return this->getSerialNumber() > other.getSerialNumber();
}

bool Item::operator==(const Item& other) const
{
	return this->getSerialNumber() == other.getSerialNumber();
}

double Item::getUnitPrice() const
{
	return this->_unitPrice;
}

std::string Item::getSerialNumber() const 
{
	return this->_serialNumber;
}

std::string Item::getNameOfTheItem() const
{
	return this->_name;
}

void Item::setCount(int count)
{

	this->_count = count;
}

int Item::getCount() const
{
	return this->_count;
}

