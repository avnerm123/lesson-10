#pragma once
#include<iostream>
#include<string>
#include<algorithm>


class Item
{
public:
	Item(std::string, std::string, double);
	Item();
	~Item();

	double totalPrice() const; //returns _count*_unitPrice
	bool operator <(const Item& other) const; //compares the _serialNumber of those items.
	bool operator >(const Item& other) const; //compares the _serialNumber of those items.
	bool operator ==(const Item& other) const; //compares the _serialNumber of those items.

	//get and set functions
	double getUnitPrice() const; 
	std::string getSerialNumber() const;
	std::string getNameOfTheItem() const;
	void setCount(int count);
	int getCount() const;


	friend std::ostream& operator<<(std::ostream& out, Item a) //operator << for cout
	{
		return out << "price: " << a.totalPrice() << "\t" << std::stoi(a.getSerialNumber()) << "." << a.getNameOfTheItem() << std::endl;
	}


private:
	std::string _name;
	std::string _serialNumber; //consists of 5 numbers
	int _count; //default is 1, can never be less than 1!
	double _unitPrice; //always bigger than 0!

};