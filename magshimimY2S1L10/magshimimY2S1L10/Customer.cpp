#include "Customer.h"

Customer::Customer(std::string name) : _name(name)
{
}

Customer::Customer()
{

}

double Customer::totalSum() const
{
	std::set<Item>::iterator it;
	double sum = 0;
	Item curr;

	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		curr = (Item)*it;
		sum += curr.totalPrice();
	}
	return sum;
}

void Customer::addItem(Item item)
{
	std::set<Item>::iterator it;
	Item a;
	for (it = this->_items.begin(); it != this->_items.end(); ++it) // going on all the set
	{
		if ((Item)(*it) == item)//if there is already such an item in the set
		{
			a = *it;//getting the item which is already in teh set
			this->_items.erase(a);//earasing him
			a.setCount(a.getCount() + 1); //incresing the number of items
			this->_items.insert(a); //inserting the new item
			return;
		}
	}
	this->_items.insert(item);
}

void Customer::removeItem(Item item)
{
	std::set<Item>::iterator it;
	Item a;
	for (it = this->_items.begin(); it != this->_items.end(); ++it) // going on all the set
	{
		if ((Item)(*it) == item) //if there is already such an item in the set
		{
			a = *it; //getting the item which is already in teh set
			this->_items.erase(a);  //earasing him
			a.setCount(a.getCount() - 1); //decrecing by 1 
			this->_items.insert(a); // inserting the new item
			return;
		}
	}
	this->_items.erase(item);
}

std::string Customer::getName() const
{
	return this->_name;
}

void Customer::setName(std::string name)
{
	this->_name = name;
}

void Customer::printItems() const
{
	std::set<Item>::iterator it;
	Item curr;
	// printing all the elements of the set 
	for (it = this->_items.begin(); it != this->_items.end(); ++it)
	{
		std::cout << (Item)*it;
	}
}

