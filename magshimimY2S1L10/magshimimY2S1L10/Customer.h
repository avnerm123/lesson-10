#pragma once
#include"Item.h"
#include<set>
#define MAX_ITEMS_SIZE 100
class Customer
{
public:
	Customer(std::string); //ctor init only the name
	Customer(); 
	double totalSum() const;//returns the total sum for payment
	void addItem(Item);//add item to the set
	void removeItem(Item);//remove item from the set

	//get and set functions
	std::string getName() const;
	void setName(std::string name);
	void printItems() const; //printing the std::set<Item> value

private:
	std::string _name;
	std::set<Item> _items;


};
