#include"Customer.h"
#include<map>
#include <iostream>

#define EXIT 4
#define ADD_COSTOMER 1
#define CHANGE_EXSITING_COSTUMER 2
#define SIZE_OF_ITEMS 10
#define EXIT_OPTION_2 3
#define ADD_ITEMS 1
#define SUM_OF_CUS_CHOICE 3
#define REMOVE_ITEM 2

using std::cout;
using std::endl;
using std::cin;

void menu();
int getPlace(std::string serialNumber);
void printItems(Item* itemList);
int getChoiceForMenu1();
int main()
{

	std::map<std::string, Customer> abcCustomers;
	int choicesTaken[SIZE_OF_ITEMS] = { 0 };
	int choice = 1, choice1 = 1;
	std::string currCustomerName;
	Item itemList[SIZE_OF_ITEMS] = {
		Item("Milk","00001",5.3),
		Item("Cookies","00002",12.6),
		Item("bread","00003",8.9),
		Item("chocolate","00004",7.0),
		Item("cheese","00005",15.3),
		Item("rice","00006",6.2),
		Item("fish", "00008", 31.65),
		Item("chicken","00007",25.99),
		Item("cucumber","00009",1.21),
		Item("tomato","00010",2.32)};

	while (choice != EXIT)
	{
		do {
			menu();
			cin >> choice;
			system("cls");
		} while (choice < ADD_COSTOMER && choice > EXIT); //this block get's the choice of the user

		switch (choice)
		{
		case ADD_COSTOMER:
		{
		
			cout << "Enter the name of the Customer:" << endl;
			cin >> currCustomerName;
			system("cls");
			while (abcCustomers.find(currCustomerName) != abcCustomers.end())
			{
				cout << "customer already exsist, please try again:" << endl;
				cin >> currCustomerName;
				system("cls");
			} //this block get's the currect name of the customer, if he dosnt already exsist
			Customer currentCustomer(currCustomerName);

			while (choice1 != 0)
			{
				cout << "The items you can buy are: (0 to exit)" << endl;
				printItems(itemList);
				cin >> choice1;
				system("cls");
				if (choice1 > 0)
				{
					currentCustomer.addItem(itemList[choice1 - 1]);
				} //this block add the item's of the current customer
			}
			choice1 = 1;
			abcCustomers[currCustomerName] = currentCustomer;
			break;
		}
		case CHANGE_EXSITING_COSTUMER:
			cout << "Enter the name of the Customer:" << endl;
			cin >> currCustomerName;
			system("cls");
			while (abcCustomers.find(currCustomerName) == abcCustomers.end())
			{
				cout << "customer does not exsist, please try again:" << endl;
				cin >> currCustomerName;
				system("cls");
			}//this block get's the currect name of the customer, if he dosnt already exsist
			cout << "items of " << currCustomerName << " are:" << endl;
			Customer* currentCustomer;
			currentCustomer = &abcCustomers[currCustomerName];
			currentCustomer->printItems(); //printing the item's of the current customer
			system("PAUSE");

			choice1 = getChoiceForMenu1();
			while (choice1 != EXIT_OPTION_2)
			{
				switch (choice1)
				{
				case ADD_ITEMS:
				{
					do
					{
						cout << "The items you can buy are: (0 to exit)" << endl;
						printItems(itemList);
						cin >> choice1;
						system("cls");
						if (choice1 > 0)
						{
							currentCustomer->addItem(itemList[choice1 - 1]);
						}
					} while ((choice1 != 0));//this block add's the new item's to the current customer

					break;
				}
				case REMOVE_ITEM:
					do 
					{
						cout << "The items you can remove are: (0 to exit)" << endl;
						printItems(itemList);
						cin >> choice1;
						system("cls");
						if (choice1 > 0)
						{
							currentCustomer->removeItem(itemList[choice1 - 1]);
						}
					}while ((choice1 != 0));//this block's removes the item's
					break;
				}
				choice1 = getChoiceForMenu1();

			}
			break;
		case SUM_OF_CUS_CHOICE:
			system("cls");
			const Customer* curr{};
			double sum = 0;
			for (auto it = abcCustomers.cbegin(); it != abcCustomers.cend(); ++it) //going through the whole map
			{
				if (it->second.totalSum() > sum) 
				{
					curr = &it->second;
					sum = curr->totalSum();
				}
			}
			cout << "The customer who pays the most is " << curr->getName() << endl;
			curr->printItems();
			system("PAUSE");
			break;
		}
	}


	return 0;
}


void menu()
{
	system("cls");
	cout << "Welcome to MagshiMart!" << endl;
	cout << "1. to sign as customer and buy items" << endl;
	cout << "2. to uptade existing customer's items" << endl;
	cout << "3. to print the customer who pays the most" << endl;
	cout << "4. to exit" << endl;
}


void menu1()
{
	system("cls");
	cout << "1. Add items" << endl;
	cout << "2. Remove items" << endl;
	cout << "3. Back to menu" << endl;
}

int getPlace(std::string serialNumber)
{
	return std::stoi(serialNumber); //"00001" will turn into 1
}

void printItems(Item* itemList)
{
	for (int i = 0; i < SIZE_OF_ITEMS; i++)
	{
		cout << itemList[i];
	}
}

int getChoiceForMenu1()
{
	int choice1;
	do
	{
		menu1();
		cin >> choice1;
		system("cls");
	} while (choice1 > EXIT_OPTION_2 || choice1 <= 0);
	return choice1;
}
